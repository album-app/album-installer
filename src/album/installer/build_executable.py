import logging
import os
import shutil
from pathlib import Path

import PyInstaller.__main__
import pkg_resources

from album.installer.install_all import force_remove


def run(args):
    logging.info("Build an executable which installs Album.")
    output_path = args.output_path
    logging.info("--output_path: %s" % output_path)
    working_dir = Path(output_path).joinpath("build")

    if not Path(output_path).is_dir():
        Path(output_path).mkdir()
    if not Path(working_dir).is_dir():
        Path(working_dir).mkdir()

    script_path = str(pkg_resources.resource_filename('album.installer', 'install_all.py'))
    env_path = str(pkg_resources.resource_filename('album.installer.resources.environments', 'album-ui.yml'))
    icons_path = str(pkg_resources.resource_filename('album.installer.resources.icons', '*'))
    exe_path_param = '--distpath=%s' % str(output_path)
    workpath_param = '--workpath=%s' % str(working_dir)
    yaml_param = '--add-data=%s%s%s' % (env_path, os.pathsep, '.')
    icons_param = '--add-data=%s%s%s' % (icons_path, os.pathsep, '.')
    hidden_imports = ["pkg_resources", "win32", "win32com", "win32api", "win32com.client", "win32com.shell",
                      "win32com.shell.shell", "win32com.shell.shellcon"]
    collect_submodules = ["win32com", "win32api"]
    pyinstaller_params = [script_path, '--onefile', '--name=album-installer', workpath_param, exe_path_param,
                          yaml_param, icons_param]
    for hidden_import in hidden_imports:
        pyinstaller_params.append("--hidden-import=%s" % hidden_import)
    for collect_module in collect_submodules:
        pyinstaller_params.append("--collect-submodules=%s" % collect_module)
    try:
        PyInstaller.__main__.run(pyinstaller_params)
    except Exception as e:
        raise RuntimeError("PyInstaller exited with an unexpected error! %s" % e) from e
    finally:
        force_remove(working_dir)

import errno
import logging
import os.path
import shutil
import stat
import subprocess
import sys
from pathlib import Path

from album.installer.install_conda import install_conda_if_missing

warned_named = False
warned_prefix = False
deleted = False


def install_album_environment(conda_executable, yml_path, environment_path):
    if Path(environment_path).exists():
        print("Removing old album environment...")
        force_remove(environment_path)
    print("Installing album environment...")
    # install_via_mamba = [conda_executable, 'run', '-n', 'base', 'mamba', 'env', 'create', '--force', '-p', environment_path, '-f', yml_path]
    install_via_conda = [conda_executable, 'env', 'create', '--force', '-p', environment_path, '-f', yml_path]
    a = subprocess.Popen(install_via_conda, stdout=sys.stdout, stderr=sys.stderr)
    a.communicate()

    if a.returncode == 0:
        print("Successfully installed album.")
    else:
        print("An error occurred installing album:")
        print(a.stderr)
        sys.exit()


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def create_shortcut(conda_executable, album_environment_path):
    p = subprocess.Popen([conda_executable, 'run', '-p', album_environment_path, 'album', 'add-shortcut'], stdout=sys.stdout, stderr=sys.stderr)
    p.communicate()


def get_conda_path(album_base_path):
    return Path(album_base_path).joinpath("Miniconda")


def get_album_base_path():
    return Path.home().joinpath('.album')


def get_album_environment_path(album_base_path):
    return Path(album_base_path).joinpath("envs", "album")


def force_remove(path, warning=True):
    path = Path(path)
    if path.exists():
        try:
            if path.is_file():
                try:
                    path.unlink()
                except PermissionError:
                    handle_remove_readonly(os.unlink, path, sys.exc_info())
            else:
                shutil.rmtree(
                    str(path), ignore_errors=False, onerror=handle_remove_readonly
                )
        except PermissionError as e:
            logging.warning("Cannot delete %s." % str(path))
            if not warning:
                raise e


def handle_remove_readonly(func, path, exc):
    """Changes readonly flag of a given path."""
    excvalue = exc[1]
    if func in (os.rmdir, os.remove, os.unlink) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)  # 0777
        func(path)
    else:
        raise

def main():
    album_base_path = get_album_base_path()
    _conda_path = get_conda_path(album_base_path)
    album_environment_path = get_album_environment_path(album_base_path)
    album_ui_yml_path = resource_path('album-ui.yml')

    logging.info("Checking for conda installation...")
    conda_executable = install_conda_if_missing(_conda_path, album_base_path)

    logging.info("Install or reinstall album environment...")
    install_album_environment(conda_executable, album_ui_yml_path, album_environment_path)

    create_shortcut(conda_executable, album_environment_path)


if __name__ == '__main__':
    main()

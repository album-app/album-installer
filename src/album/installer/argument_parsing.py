import argparse


def main():
        parser = argparse.ArgumentParser()
        parser.add_argument('output_path', type=str, help='Path where the package solution should be written to.')
        args = parser.parse_args()
        from album.installer.build_executable import run
        run(args)

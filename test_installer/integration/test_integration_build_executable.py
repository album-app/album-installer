import os
import platform
import subprocess
import unittest
from argparse import Namespace
from pathlib import Path
from tempfile import mkdtemp

from src.album.installer import build_executable


class TestIntegrationBuildExecutable(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = mkdtemp()

    def tearDown(self) -> None:
        super().tearDown()
        
    def test_build_executable(self):
        # build an executable and try running it
        build_executable.run(Namespace(output_path=self.tmp_dir))

        self.assertTrue(Path(self.tmp_dir).is_dir(),
                        "Output directory was not created.")

        self.assertFalse(Path(self.tmp_dir).joinpath('build').is_dir(),
                         "PyInstallers build directory was not deleted.")

        if platform.system() == 'Windows':
            exe_path = Path(self.tmp_dir).joinpath('album-installer.exe')
        else:
            exe_path = Path(self.tmp_dir).joinpath('album-installer')

        self.assertTrue(exe_path.exists())
        # cmd = [str(exe_path)]
        # p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, stdout=subprocess.STDOUT)
        # p.communicate()
        #
        # self.assertTrue(Path(exe_path).is_file() and os.access(exe_path, os.X_OK),
        #                 "No executable created or executable not accessible.")
        # self.assertEqual(0, p.returncode, "Executable could not be executed correctly.")


if __name__ == '__main__':
    unittest.main()

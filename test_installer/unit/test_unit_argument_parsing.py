import sys
import unittest.mock
from argparse import Namespace
from unittest.mock import patch

from album.installer import argument_parsing


class TestUnitArgumentParsing(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()

    @patch('album.installer.build_executable.run')
    def test_create_parser(self, _run):
        sys.argv = ["album-installer", "output_somewhere"]
        argument_parsing.main()
        _run.assert_called_once_with(Namespace(output_path="output_somewhere"))


if __name__ == '__main__':
    unittest.main()

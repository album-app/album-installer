import unittest.mock
from argparse import Namespace
from pathlib import Path
from tempfile import mkdtemp
from unittest.mock import patch

from album.installer import build_executable


class TestUnitBuildExecutable(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()

    @patch('PyInstaller.__main__.run')
    def test_run(self, _run):
        tmp_dir = mkdtemp()
        build_executable.run(Namespace(output_path=tmp_dir))
        pyinstaller_args = _run.call_args[0][0]
        self.assertIn("--onefile", pyinstaller_args)
        self.assertIn("--name=album-installer", pyinstaller_args)
        self.assertIn("--distpath=%s" % tmp_dir, pyinstaller_args)
        working_dir = Path(tmp_dir).joinpath("build")
        self.assertIn("--workpath=%s" % working_dir, pyinstaller_args)
        self.assertFalse(working_dir.exists())


if __name__ == '__main__':
    unittest.main()

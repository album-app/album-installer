import time
import unittest

from test_installer.unit import test_unit_build_executable, test_unit_argument_parsing, test_unit_install_all, \
    test_unit_install_conda


def main():
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    suite.addTests(loader.loadTestsFromModule(test_unit_argument_parsing))
    suite.addTests(loader.loadTestsFromModule(test_unit_build_executable))
    suite.addTests(loader.loadTestsFromModule(test_unit_install_all))
    suite.addTests(loader.loadTestsFromModule(test_unit_install_conda))

    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(suite)
    if result.wasSuccessful():
        time.sleep(5)
        print("Success")
        exit(0)
    else:
        print("Failed")
        exit(1)


main()
